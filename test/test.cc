/*
    bmojp
    Copyright (C) 2019  bademo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define DEBUG
#include <string>
#include <fstream>
#include <iostream>

#include "../bmojp.h"
using namespace std;

int main(int argc,char * argv[]){
  using namespace Bmojp;
  ifstream infile("./test/json.json");
  string json((istreambuf_iterator<char>(infile)),istreambuf_iterator<char>());
  infile.close();
  
  bmojson *hi = bmojp::parse(json);

  cout << bmojp::parse(hi->stringify())->stringify() << "\n" << hi->stringify();
  cout << "\n" << hi->getType("a") << "\n";
  cout << "`" << hi->getJson("a")->getString("b") << "`" << endl;
  if(bmojp::parse(hi->stringify())->stringify() != hi->stringify()){
    return EXIT_FAILURE;
  }

}