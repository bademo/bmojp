# "is_bmojp_cool": true

## Orentation

### How-to

1. First off, please include the `bmojp.h` header

```cpp
include "bmojp.h"
```

2. To parse a JSON call (`bmojp::bmojp::parse()`)

```cpp
#include <string>
#include <fstream>
#include <iostream>
#include "../bmojp.h"
using namespace std;
using namespace Bmojp;

int main(){
  ifstream infile("./test/json.json");
  string json((istreambuf_iterator<char>(infile)),istreambuf_iterator<char>());
  infile.close();

  bmojson *hi = bmojp::parse(json);
}
```

3. To get values from the JSON, first check what type of value it is 

```cpp
int type = json->getType("key")
```

### Notes - getType

* **0** means that the key does not exist
* The rest of the *values* are defined in the enum jsontypes in `bmojp.h`

## Notes - compiling
* remember to link against bmojp.cc when compiling
* remember file paths are relative to cwd so run test.exe from root directory
* if compiling is failing use mingw g++ on windows try updating your g++

## Simple defines

* If you would like a simpler method of calling them define `__BMOJP_SIMPLE_DEFINES__` before including `bmojp.h`
* It adds the macro `parseJSON(toParse)`, which expands to `Bmojp::bmojp::parse(toParse)` and `jsonObj` which expands to `Bmojp::bmojson`

# Contributing
to contribute fork the repo and check out [todo.md](todo.md)
goals are ordered top->bottom in order of importance, so work on higher goals first
once you've finished working on it submit a merge request!