/*
    bmojp
    Copyright (C) 2019  bademo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "bmojp.h"

namespace Bmojp
{

#pragma region bmojson

bmojson::bmojson(){};

bmojson::bmojson(map<string, string> ss)
{
    strings = ss;
};
bmojson::bmojson(map<string, int> is)
{
    ints = is;
};
bmojson::bmojson(map<string, int> is, map<string, string> ss)
{
    ints = is;
    strings = ss;
};



string bmojson::stringify(){
    string ret = "{";
    for(const auto &iterator : strings){
        ret += "\"" + iterator.first + "\":\"" + iterator.second + "\",";
    }
    for(const auto &iterator : ints){
        ret += "\"" + iterator.first + "\":" + to_string(iterator.second) + ",";
    }
    // hack so i don't have to implement iterators into Bmojp::bmojson
    for(const auto &myPair : jsons){
    //    cout << myPair.second->stringify() << "\n";
        ret += "\"" + myPair.first + "\":" + myPair.second->stringify() + ",";
    }
    ret[ret.length()-1] = '}';
    return ret;
}


void bmojson::setValue(string key, string value)
{
    strings.insert(map<string, string>::value_type(key, value));
};
void bmojson::setValue(string key, int value)
{
    ints.insert(map<string, int>::value_type(key, value));
};
void bmojson::setValue(string key, bmojson *value)
{
    jsons.insert(map<string, bmojson *>::value_type(key, value));
};

int bmojson::getInt(string key)
{
    return ints.at(key);
};
string bmojson::getString(string key)
{
    return strings.at(key);
};
bmojson *bmojson::getJson(string key)
{
    return jsons.at(key);
};

map<string, string> *bmojson::getStrings()
{
    return &strings;
}
map<string, int> *bmojson::getInts()
{
    return &ints;
}
map<string, bmojson *> *bmojson::getJsons()
{
    return &jsons;
}

int bmojson::getType(string key)
{
    if (strings.find(key) != strings.end())
    {
        return (int)jsontype::STRING;
    }
    else if (ints.find(key) != ints.end())
    {
        return (int)jsontype::INT;
    }
    else if (jsons.find(key) != jsons.end())
    {
        return (int)jsontype::JSON;
    }
    else
    {
        return (int)jsontype::NONE;
    }
}

#pragma endregion

#pragma region parser
bmojson *bmojp::parse(string toParse)
{
    bool isString = false;
    string nextParse = "";
    // preproccesses and removes all whitespaces outside of strings/keys
    for(int i = 0; i < toParse.length();i++){
        char curChar = toParse[i];
        if(curChar == '\"' && toParse[i-1] != '\\'){
            isString = !isString;
        }
        if(!(curChar == '\n' || curChar == ' ' || curChar == '\t' || curChar == '\r' || curChar == '\v') || isString){
            nextParse += curChar;
        }
    }


    bmojson *json = new bmojson();
    // skips unnecessary characters
    bool hasStarted = false;
    // used when nesting json is supported
    int layer = 0;
    // check if it is a key currently
    bool inKey = false;
    // check if it's a string currently
    bool inString = false;
    // check if it's in a number
    bool inNumber = false;
    // check if it's in a nested json
    bool inJson = false;
    string curNumber = "";
    string curKey = "";
    string curString = "";
    string curJson = "";
    char lastChar;

    for (int i = 0; i <= nextParse.length(); i++)
    {
        char curChar = nextParse[i];
        if ((curChar != '\n' && curChar != ' ') || (inString || inKey || inJson))
        {
            // skips until it finds an opening bracket
            if (!hasStarted)
            {
                if (curChar == '{')
                {
                    hasStarted = true;
                    lastChar = '{';
                }
            }
            else
            {

                if (curChar == '\"' && nextParse[i + 1] == ':')
                {
                    inKey = false;
                }
                else if ((nextParse[i + 1] == ',' || nextParse[i + 1] == '}') && curChar == '\"' && inString)
                {
                    inString = false;
                    int type = json->getType(curKey);
                    
                    if (type != 0)
                    {
                        switch ((jsontype)type)
                        {
                        case jsontype::STRING:
                            json->getStrings()->erase(curKey);
                            break;
                        case jsontype::INT:
                            json->getInts()->erase(curKey);
                            break;
                        case jsontype::JSON:
                            json->getJsons()->erase(curKey);
                        }
                    }

                    json->setValue(curKey, curString);
                    curKey = "";
                    curString = "";
                }
                else if ((curChar == ',' || curChar == '}') && inNumber)
                {
                    inNumber = false;

                    int type = json->getType(curKey);
                    if (type != 0)
                    {
                        switch ((jsontype)type)
                        {
                        case jsontype::STRING:
                            json->getStrings()->erase(curKey);
                            
                            break;
                        case jsontype::INT:
                            json->getInts()->erase(curKey);
                            
                            break;
                        case jsontype::JSON:
                            json->getJsons()->erase(curKey);
                        }
                    }

                    json->setValue(curKey, stoi(curNumber));
                    curKey = "";
                    curNumber = "";
                }
                else if (lastChar == ':' && !inString && !inKey && !inNumber && curKey != "" && curString == "" && !inJson)
                {
                    if (curChar == '0' || curChar == '1' || curChar == '2' || curChar == '3' || curChar == '4' || curChar == '5' || curChar == '6' || curChar == '7' || curChar == '8' || curChar == '9')
                    {
                        inNumber = true;
                    }
                }
                if (lastChar == ':' && !inString && !inKey && !inJson && !inNumber && curKey != "" && curString == "" && curChar == '{')
                {
                    inJson = true;
                }

                if (inJson)
                {
                    curJson += curChar;
                }
                else if (inKey)
                {
                    curKey += curChar;
                }
                else if (inString)
                {
                    curString += curChar;
                }
                else if (inNumber)
                {
                    curNumber += curChar;
                }

                if (curChar == '}' && (nextParse[i + 1] == ',' || nextParse[i + 1] == '}') && inJson && layer == 1)
                {

                    int type = json->getType(curKey);
                    if (type != 0)
                    {
                        switch ((jsontype)type)
                        {
                        case jsontype::STRING:
                            json->getStrings()->erase(curKey);
                            break;
                        case jsontype::INT:
                            json->getInts()->erase(curKey);
                            break;
                        case jsontype::JSON:
                            json->getJsons()->erase(curKey);
                        }
                    }

                    inJson = false;

                    bmojson *toAdd = parse(curJson);
                    json->setValue(curKey, toAdd);
                    
                    curKey = "";
                    curJson = "";
                }

                if (curChar == '}' && !inString && !inKey)
                {
                    layer--;
                }
                else if (curChar == '{' && !inString && !inKey)
                {
                    layer++;
                }
                else if ((curChar == '\"' && (!inString && !inKey && !inJson && curKey == "") && curString == "") && (lastChar == ',' || lastChar == '{'))
                {
                    inKey = true;
                }
                else if (curChar == '\"' && lastChar == ':' && !inString && !inJson && !inKey && curKey != "")
                {
                    inString = true;
                }

                if (i != nextParse.length() && layer < 0)
                {
                    break;
                }
                
                if (curChar == ' ' || curChar == '\n' && !inString && !inKey)
                {
                    curChar = lastChar;
                }
                
                lastChar = curChar;
            }
        }
    }

    return json;
};
#pragma endregion

} // namespace bmojp