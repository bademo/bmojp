/*
    bmojp
    Copyright (C) 2019  bademo

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
    
    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once
#ifndef FILE__BMOJP__LOCK
#define FILE__BMOJP__LOCK

#ifdef __BMOJP_SIMPLE_DEFINES__
#define parseJSON(toParse) Bmojp::bmojp::parse(toParse)
#define jsonObj Bmojp::bmojson
#endif

#include <map>
#include <string>
#include <iostream>

namespace Bmojp
{
using namespace std;

class bmojson
{
  public:
    bmojson(map<string, int> is, map<string, string> ss);
    bmojson(map<string, int> is);
    bmojson(map<string, string> ss);
    bmojson();

   string stringify();

    string getString(string key);
    int getInt(string key);
    bmojson *getJson(string key);

    void setValue(string key, string value);
    void setValue(string key, int value);
    void setValue(string key, bmojson *value);

    map<string, int> *getInts();
    map<string, string> *getStrings();
    map<string, bmojson *> *getJsons();

    int getType(string key);

  private:
    map<string, bmojson *> jsons;
    map<string, int> ints;
    map<string, string> strings;
};

typedef enum
{
    NONE = 0,
    JSON = 1,
    INT = 2,
    STRING = 3
} jsontype;

class bmojp
{
  public:
    static bmojson *parse(string toParse);
};

} // namespace bmojp

#endif